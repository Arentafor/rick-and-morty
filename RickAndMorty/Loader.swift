//
//  Loader.swift
//  RickAndMorty
//
//  Created by Виталий Крюков on 25.05.2021.
//

import Foundation
import Alamofire

protocol AlomafireLoadDelegate {
    func alLoaded(AlomafireData:[AlomafireDatas])
}

class LoaderCharacter {
    
    var delegate: AlomafireLoadDelegate?
    
    func AlomafireLoader() {
        AF.request("https://rickandmortyapi.com/api/character").responseJSON { data in
            if let objects = data.value,
            let jsonDict = objects as? NSDictionary,
            let jsonArray = jsonDict["results"] as? NSArray {
                DispatchQueue.main.async {
                    var categories: [AlomafireDatas] = []
                    for dict in jsonArray{
                        if let category = AlomafireDatas(data: dict as! NSDictionary){
                            categories.append(category)
                        }
                    }
                    self.delegate?.alLoaded(AlomafireData: categories)
                }
            }
        }
    }
}

class AlomafireDatas {

    var name: String
    var species: String
    var status: String
    var location: String
    var image: String

    init?(data: NSDictionary){

        guard let name = data["name"] as? String,
            let status = data["status"] as? String,
            let species = data["species"] as? String,
            let containerLocation = data["location"] as? NSDictionary,
            let location = containerLocation["name"] as? String,
            let image = data["image"] as? String else {print("errror"); return nil}

        self.name = name
        self.species = species
        self.status = status
        self.location = location
        self.image = image
    }
}

