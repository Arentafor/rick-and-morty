//
//  CharacterViewController.swift
//  RickAndMorty
//
//  Created by Виталий Крюков on 26.05.2021.
//

import UIKit
import Alamofire

class CharacterViewController: UIViewController {
            
    @IBOutlet var characterImage: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var liveStatusLabel: UILabel!
    
    @IBOutlet var lastKnowLocatonLabel: UILabel!
    
    @IBOutlet var firstSeenLocationLabel: UILabel!
   
    var index: Int = 98
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadDataCharacter()
    }
    
    func loadDataCharacter() {
        AF.request("https://rickandmortyapi.com/api/character/\(index + 1)").responseJSON { data in

            if let objects = data.value{

                DispatchQueue.main.async {
                  let jsondict = objects as! NSDictionary

                    if let url = URL(string: jsondict["image"] as! String) {
                        if let data = NSData(contentsOf: url) {
                            
                            self.characterImage.image = UIImage(data: data as Data)
                        }
                    }
                    self.nameLabel.text = jsondict["name"] as? String
                    self.liveStatusLabel.text = jsondict["status"] as? String
                    let contLastKnowLocatonLabel = jsondict["location"] as! NSDictionary
                    self.lastKnowLocatonLabel.text = contLastKnowLocatonLabel["name"] as? String
                    let contfirstSeenLocation = jsondict["origin"] as! NSDictionary
                    self.firstSeenLocationLabel.text = contfirstSeenLocation["name"] as? String
                }
            }
        }
    }
}
    
