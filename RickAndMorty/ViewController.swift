//
//  ViewController.swift
//  RickAndMorty
//
//  Created by Виталий Крюков on 25.05.2021.
//

import UIKit

class ViewController: UIViewController {
    
    var index = 0
    
    @IBOutlet var tableView: UITableView!
    
    var data:[AlomafireDatas] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        let alLoader = LoaderCharacter()
        alLoader.delegate = self
        alLoader.AlomafireLoader()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? CharacterViewController else { return }
        destination.index = index
    }
}

extension ViewController: AlomafireLoadDelegate {
    func alLoaded(AlomafireData: [AlomafireDatas]) {
        self.data = AlomafireData
        tableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        let pathRow = data[indexPath.row]
        cell.LastKnownLocationLabel.text = pathRow.location
        cell.nameLabel.text = pathRow.name
        cell.statusSpeciesLabel.text = "\(pathRow.status) - \(pathRow.species)"
        if pathRow.status == "Alive"{
            cell.ControlLiveLabel.text = "🟢"
        }
        else {cell.ControlLiveLabel.text = "🔴" }

        if let url = URL(string: pathRow.image) {
            if let data = NSData(contentsOf: url) {
                cell.charaterImage.image = UIImage(data: data as Data)
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let ind = storyboard!.instantiateViewController(identifier: "showmore") as? CharacterViewController else { return }
        ind.index = indexPath.row
        present(ind, animated: true, completion: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
    


