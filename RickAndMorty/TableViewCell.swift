//
//  TableViewCell.swift
//  RickAndMorty
//
//  Created by Виталий Крюков on 25.05.2021.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var charaterImage: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var statusSpeciesLabel: UILabel!
    
    @IBOutlet var ControlLiveLabel: UILabel!
    
    @IBOutlet var LastKnownLocationLabel: UILabel!
}
